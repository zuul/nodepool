# Copyright 2018 Red Hat
# Copyright 2023 Acme Gating, LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
#
# See the License for the specific language governing permissions and
# limitations under the License.

from copy import deepcopy
import math

import voluptuous as v

from nodepool.driver.openshift.config import OpenshiftPool
from nodepool.driver.openshift.config import OpenshiftProviderConfig


class OpenshiftPodsProviderConfig(OpenshiftProviderConfig):

    def __eq__(self, other):
        if isinstance(other, OpenshiftPodsProviderConfig):
            return (super().__eq__(other) and
                    other.context == self.context and
                    other.pools == self.pools)
        return False

    def set_max_servers(self):
        # We translate max-pods to max_servers to re-use quota
        # calculation methods.
        self.max_servers = self.provider.get(
            'max-pods',
            self.provider.get('max-servers',
                              math.inf
            )
        )

    def set_pools(self, config):
        for pool in self.provider.get('pools', []):
            # Force label type to be pod
            labels = pool.get('labels', [])
            if len(labels) > 0:
                for i in range(len(labels)):
                    pool['labels'][i]['type'] = 'pod'
            pp = OpenshiftPool()
            pp.provider = self
            pp.load(pool, config)
            self.pools[pp.name] = pp

    def getPoolLabels(self):
        openshiftpods_label_from_nodepool = deepcopy(
            self.base_label_from_nodepool)
        openshiftpods_label_from_nodepool.update({
            v.Required('name'): str,
            v.Required('image'): str,
        })
        openshiftpods_label_from_user = deepcopy(
            self.base_label_from_user)
        openshiftpods_label_from_user.update({
            v.Required('name'): str,
            v.Required('spec'): dict,
        })
        return [v.Any(openshiftpods_label_from_nodepool,
                      openshiftpods_label_from_user)]

    def getSchemaDict(self):
        schema = super().getSchemaDict()
        schema.update({
            v.Exclusive('max-pods',
                        'max-projects-or-servers'): int,
        })
        schema.pop(v.Exclusive('max-projects',
                               'max-projects-or-servers'), None)
        return schema
